<?php

use App\Http\Controllers\LibraryCardController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\AuthorController;
use App\Http\Controllers\BookController;
use App\Http\Controllers\FeedbackController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware('auth')->controller(FeedbackController::class)->group(function () {
    Route::any('/feedback', 'index');
});

Route::controller(UserController::class)->group(function () {
    Route::any('/login', 'login')->name('login');
    Route::any('/registration', 'registration');
});

Route::middleware('auth')->controller(LibraryCardController::class)->group(function () {
    Route::any('/book/take', 'takeBook');
   // Route::any('/book/library', 'show');
});
