<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('library_cards', function (Blueprint $table) {
            $table->integer('book_item_id');
        });

        Schema::create('book_item', function (Blueprint $table) {
            $table->id();
            $table->integer('book_id');
            $table->string('revision_number');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('library_cards', function (Blueprint $table) {
            $table->dropColumn('book_item_id');
        });
        Schema::dropIfExists('book_item');
    }
};
