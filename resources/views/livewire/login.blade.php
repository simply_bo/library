<div>
    <form wire:submit.prevent="login">
        @csrf
        Email: <input type="email" name="email" value="{{ old('email') }}" wire:model="email">
        <br>

        Пароль: <input type="password" name="password" id="password" wire:model="password">
        <br>

        <button type="submit">Login</button>
    </form>
</div>
