<div>
    <form wire:submit.prevent="takeBook">
        @csrf
        ФИО:
        <input type="text" name="name" value="{{ old('name') }}" wire:model="name">
        <br>

        Телефон: <input type="number" name="phone" value="{{ old('phone') }}" wire:model="phone">
        <br>

        Адрес: <input type="text" name="address" wire:model="address">
        <br>

        <select wire:model.defer="bookId">
            <option value="null"> Select book</option>
            @foreach($books as $book)
                Выбери книгу:
                <option value="{{$book->id}}"  wire:key="{{$book->id}}" >{{ $book->title }}</option>
                <br>
            @endforeach
        </select>
        @error('book') <div class="error">{{ $message }}</div> @enderror
        <button type="submit">Оформить книгу</button>
        <br><br>
    </form>

    <form wire:submit.prevent="returnBook">
        @csrf
        <select wire:model.defer="takingBook">
            <option value="null"> Select book</option>
            @foreach($takingBooks as $card)
                <option value="{{$card->id}}" wire:key="{{$card->id}}" >{{ $card->book->title }} ({{$card->bookItem->revision_number}})</option>
                <br>
            @endforeach
        </select>
        <button type="submit">Вернуть книгу</button>
    </form>
    <form wire:submit.prevent="extendTime">
        @csrf
        <select wire:model.defer="takingBook">
            <option value="null"> Select book</option>
            @foreach($takingBooks as $card)
                <option value="{{$card->id}}" wire:key="{{$card->id}}">
                    {{ $card->book->title }}
                    ({{$card->bookItem->revision_number}}
                    {{$card->return_time}})
                </option>
                <br>
            @endforeach
        </select>
        <button type="submit">Продлить бронь</button>
    </form>

</div>
