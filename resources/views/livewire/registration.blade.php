<div>

    <form wire:submit.prevent="registration">
        @csrf
        Имя пользователя:
        <input type="text" name="name" value="{{ old('name') }}" wire:model="name">
        <br>

        Email: <input type="email" name="email" value="{{ old('email') }}" wire:model="email">
        <br>

        Пароль: <input type="password" name="password" id="password" wire:model="password">
        <br>

        <button type="submit">Register</button>
    </form>

</div>
