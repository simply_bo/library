<?php
/**
 * @var bool $isSent
 */
?>
@if($isSent)
    Ваш фидбек был отправлен
@else
    <form method="post">
        @csrf
        <input name="fio" type="text" placeholder="ФИО"><br><br>
        <input name="email" type="text" placeholder="email"><br><br>
        <input name="comment" type="text" placeholder="Комментарий"><br><br>
        <input type="submit">
    </form>
@endif
