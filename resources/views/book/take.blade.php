<?php
/**
 * @var \App\Models\Book[] $books
 * @var \App\Models\LibraryCard[] $takingBooks
 */
?>
<html>
<head>
    @livewireStyles
</head>
<body>
<livewire:counter :takingBooks="$takingBooks" :user="$user" :books="$books" />
@livewireScripts
</body>
</html>
