<?php

namespace App\Http\Controllers;

use App\Mail\OrderShipped;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class FeedbackController extends Controller
{
    public function index(Request $request)
    {
        $isSent = false;
        $comment = $request->post("comment");
        $fio = $request->post("fio");
        $email = $request->post("email");

        if ($comment != null && $fio != null && $email != null) {
            Mail::to(env('FEEDBACK_EMAIL'))->send(new OrderShipped($fio, $email, $comment));
            $isSent = true;
        }

        return view('feedback', [
            'isSent' => $isSent
        ]);
    }
}
