<?php

namespace App\Http\Controllers;

use App\Models\Book;
use App\Models\BookItem;
use App\Models\LibraryCard;
use Carbon\Carbon;
use DateInterval;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LibraryCardController extends Controller
{
    public function takeBook(Request $request)
    {
        $books = Book::all();
        $user = Auth::user();

        return view('book/take', [
            'books' => $books,
            'takingBooks' => LibraryCard::query()->where('user_id', $user->id)->get(),
            'user' => $user
        ]);
    }

    public function show()
    {
        $user = Auth::user();
        $library = LibraryCard::query()->where(['user_id' => $user->id])->get();
        return view('/book/library', [
            'libraries' => $library
        ]);
    }
}
