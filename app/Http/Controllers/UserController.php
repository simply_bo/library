<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function login(Request $request)
    {
        return view('/login');
    }

    public function registration(Request $request)
    {
        $user = new User();

        if (null !== $request->post('name')) {
            $password = $request->post('password');
            $user->fill([
                'name' => $request->post('name'),
                'email' => $request->post('email'),
                'password' => bcrypt($password),
            ]);
            $user->save();

            return redirect()->intended('/book/take');
        }
        return view('/registration');
    }
}

