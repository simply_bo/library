<?php

namespace App\Http\Livewire;

use App\Models\BookItem;
use App\Models\LibraryCard;
use App\Models\User;
use DateInterval;
use DateTime;
use Illuminate\Database\Eloquent\Collection;
use Livewire\Component;

class Counter extends Component
{
    /** @var LibraryCard[] */
    public Collection $takingBooks;
    public $takingBook;
    public User $user;
    public $bookId;
    public $name;
    public $phone;
    public $address;
    public $books;

    public function render()
    {
        return view('livewire.counter');
    }

    public function returnBook()
    {
        $deleted = LibraryCard::destroy($this->takingBook);
        $this->takingBooks = LibraryCard::query()->where('user_id', $this->user->id)->get();
        $this->emitSelf('something');
    }

    public function extendTime()
    {
        $libraries = LibraryCard::query()->where('id', $this->takingBook)->first();

        $newReturnTime = (new DateTime())->setTimestamp(strtotime($libraries->return_time))
            ->add(new DateInterval('P30D'))->format('Y-m-d H:i:s');

        $libraries->fill([

            'return_time' => $newReturnTime
        ]);

        $libraries->save();

        $this->takingBooks = LibraryCard::query()->where('user_id', $this->user->id)->get();
        $this->emitSelf('something');
    }

    public function takeBook()
    {
        $libraries = new LibraryCard();
        $bookItemId = BookItem::searchFreeRevisionNumber($this->bookId);
        if ($bookItemId == null) {
            $this->addError('book', 'Эти книги были разобраны');
            return;
        }

        $libraries->fill([
            'book_id' => $this->bookId,
            'user_id' => $this->user->id,
            'full_name' => $this->name,
            'phone' => $this->phone,
            'address' => $this->address,
            'book_item_id' => $bookItemId,
            'return_time' => (new DateTime())->add(new DateInterval('P30D'))->format('Y-m-d H:i:s'),
        ]);
        $libraries->save();

        $this->takingBooks = LibraryCard::query()->where('user_id', $this->user->id)->get();
        $this->emitSelf('something');
    }
}
