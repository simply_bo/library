<?php

namespace App\Http\Livewire;

use App\Models\User;
use Livewire\Component;

class Registration extends Component
{
    public User $user;
    public $password;
    public $name;
    public $email;

    public function render()
    {
        return view('livewire.registration');
    }

    public function registration()
    {
        $user = new User();

        $user->fill([
            'name' => $this->name,
            'email' => $this->email,
            'password' => bcrypt($this->password),
        ]);
        $user->save();

        return redirect()->intended('/book/take');
    }
}
