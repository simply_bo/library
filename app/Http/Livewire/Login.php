<?php

namespace App\Http\Livewire;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class Login extends Component
{
    public $password;
    public $email;

    public function mount() {
        $this->user = Auth::user();
    }

    public function render()
    {
        return view('livewire.login');
    }

    public function login()
    {
        $credentials = $this->validate([
            'email' => ['required', 'email'],
            'password' => ['required'],
        ]);

        if (Auth::attempt($credentials)) {

            return redirect()->intended('/book/take');
        }
        return redirect('/registration');
    }
}
