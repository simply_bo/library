<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Mail\Mailables\Address;
use Illuminate\Mail\Mailables\Content;
use Illuminate\Mail\Mailables\Envelope;
use Illuminate\Queue\SerializesModels;

class OrderShipped extends Mailable
{
    use Queueable, SerializesModels;

    private string $fio;
    private string $email;
    private string $comment;

    /**
     * Create a new message instance.
     *
     * @param string $fio
     * @param string $email
     * @param string $comment
     */
    public function __construct(string $fio, string $email, string $comment)
    {
        $this->fio = $fio;
        $this->email = $email;
        $this->comment = $comment;
    }

    /**
     * Get the message envelope.
     *
     * @return \Illuminate\Mail\Mailables\Envelope
     */
    public function envelope()
    {
        $address = new Address('admin@storeez.app', 'Отправитель');
        $envelope = new Envelope($address);
        $envelope->subject = 'Тема письма';
        return $envelope;
    }

    /**
     * Get the message content definition.
     *
     * @return \Illuminate\Mail\Mailables\Content
     */
    public function content()
    {
        $content = new Content('emails.feedback');
        $content
            ->with('fio', $this->fio)
            ->with('email', $this->email)
            ->with('comment', $this->comment);
        return $content;
    }

    /**
     * Get the attachments for the message.
     *
     * @return array
     */
    public function attachments()
    {
        return [];
    }
}
