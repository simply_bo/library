<?php

namespace App\Admin\Actions;

use Encore\Admin\Actions\RowAction;
use Illuminate\Http\Request;

class OpenBookRevisionAction extends RowAction
{
    public $name = 'Open revision list';

    public function href()
    {
        return '/admin/book_item?book_id=' . $this->row->id;
    }
}
