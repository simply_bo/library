<?php

namespace App\Admin\Controllers;

use App\Models\Book;
use App\Models\BookItem;

use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Grid\Filter;
use Encore\Admin\Show;


class BookItemController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'BookItem';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new BookItem());
        $grid->filter(function (Filter $filter) {
            $filter->equal('book_id', 'Book')->select(Book::all()->pluck('title', 'id'));
        });
        $grid->column('id', __('Id'));
        $grid->column('book_id', __('Book id'));
        $grid->column('revision_number', __('Revision number'));
        $grid->column('created_at', __('Created at'));
        $grid->column('updated_at', __('Updated at'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(BookItem::findOrFail($id));


        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new BookItem());

        $form->select('book_id', 'Book id')->options(Book::all()->pluck('title', 'id'));
        $form->text('revision_number', __('Revision number'));

        return $form;
    }
}
