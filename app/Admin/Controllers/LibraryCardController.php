<?php

namespace App\Admin\Controllers;

use App\Models\LibraryCard;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class LibraryCardController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'LibraryCard';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new LibraryCard());

        $grid->column('id', __('Id'));
        $grid->column('user', __('User'))->display(function ($user){
            return $user['email'];
        });
        $grid->column('book', __('Book'))->display(function ($book){
            return $book['title'];
        });
        $grid->column('full_name', __('Full name'));
        $grid->column('phone', __('Phone'));
        $grid->column('address', __('Address'));
        $grid->column('created_at', __('Date of taking'));
        $grid->column('return_time', __('Return date'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(LibraryCard::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('user_id', __('User id'));
        $show->field('book_id', __('Book id'));
        $show->field('full_name', __('Full name'));
        $show->field('phone', __('Phone'));
        $show->field('address', __('Address'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new LibraryCard());

        $form->number('user_id', __('User id'));
        $form->number('book_id', __('Book id'));
        $form->text('full_name', __('Full name'));
        $form->mobile('phone', __('Phone'));
        $form->text('address', __('Address'));

        return $form;
    }
}
