<?php

namespace App\Admin\Controllers;

use App\Admin\Actions\OpenBookRevisionAction;
use App\Models\Author;
use App\Models\Book;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class BookController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Book';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Book());
        $grid->column('id', __('Id'));
        $grid->column('title', __('Title'));
        $grid->column('year', __('Year'));
        $grid->column('items', __('Count'))->display(function ($items) {
            return count($items);
        });
        $grid->column('cards', __('On hands count'))->display(function ($cards) {
            return count($cards);
        });
        $grid->column('_', __('Free book count'))->view('admin/free');
        $grid->column('created_at', __('Created_at'));
        $grid->column('updated_at', __('Updated_at'));
        $grid->actions(function ($actions) {
            $actions->add(new OpenBookRevisionAction());
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Book::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('title', __('Title'));
        $show->field('year', __('Year'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Book());

        $form->text('title', __('Title'));
        $form->number('year', __('Year'));
        $form->multipleSelect('authors', 'Author')
            ->options(Author::all()->pluck('full_name', 'id'));


        return $form;
    }
}
