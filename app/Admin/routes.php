<?php



use Illuminate\Routing\Router;

Admin::routes();

Route::group([
    'prefix'        => config('admin.route.prefix'),
    'namespace'     => config('admin.route.namespace'),
    'middleware'    => config('admin.route.middleware'),
    'as'            => config('admin.route.prefix') . '.',
], function (Router $router) {

    $router->get('/', 'HomeController@index')->name('home');
    $router->resource('books', BookController::class);
    $router->resource('authors', AuthorController::class);
    $router->resource('users', UserController::class);
    $router->resource('libraries', LibraryCardController::class);
    $router->resource('book_item', BookItemController::class);

});
