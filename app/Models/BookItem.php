<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class BookItem extends Model
{
    protected $table = 'book_item';
    protected $fillable = [
        'book_id',
        'revision_number',
    ];

    public static function searchFreeRevisionNumber($bookId)
    {

        /*
         * SELECT book_item.id FROM book_item
         * LEFT JOIN library_cards ON book_item.id = library_cards.book_item_id
         * WHERE book_item.book_id = 1 AND library_cards.book_item_id is NULL
         * LIMIT 1
         *
         */

        $bookItem = DB::table('book_item')
            ->select('book_item.id')
            ->leftJoin('library_cards', 'book_item.id', '=', 'library_cards.book_item_id')
            ->where(['book_item.book_id' => $bookId])
            ->whereNull('library_cards.book_item_id')
            ->first();
        return $bookItem !== null ? $bookItem->id : null;
    }
}

