<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    protected $table = 'books';

    protected $fillable = [
        'title',
        'year',
        'count'
    ];

    public function authors()
    {
        return $this->belongsToMany(
            Author::class,
            'books_authors',
            'book_id',
            'author_id'
        );
    }

    public function items()
    {
        return $this->hasMany(
            BookItem::class,
            'book_id',
            'id'
        );
    }

    public function cards()
    {
        return $this->hasMany(
            LibraryCard::class,
            'book_id',
            'id'
        );
    }
}
