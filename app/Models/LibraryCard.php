<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LibraryCard extends Model
{
    protected $table = 'library_cards';

    protected $fillable = [
        'user_id',
        'book_id',
        'full_name',
        'phone',
        'address',
        'book_item_id',
        'return_time'
    ];

    public function book()
    {
        return $this->hasOne(
            Book::class,
            'id',
            'book_id'
        );
    }

    public function bookItem()
    {
        return $this->hasOne(
            BookItem::class,
            'id',
            'book_item_id'
        );
    }

    public function user()
    {
        return $this->hasOne(
            User::class,
            'id',
            'user_id'
        );
    }

}
