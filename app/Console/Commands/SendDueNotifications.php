<?php

namespace App\Console\Commands;

use App\Mail\DueNotification;
use App\Models\LibraryCard;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class SendDueNotifications extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:due';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sending to the clients mail about the notice of the debt';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        /** @var LibraryCard[] $debtors */
        $debtors = LibraryCard::query()->where('return_time', '<=', Carbon::now())->get();
        echo count($debtors) . PHP_EOL;
        foreach ($debtors as $debtor) {
            Mail::to($debtor->user->email)->send(new DueNotification);
        }
        return Command::SUCCESS;
    }
}
